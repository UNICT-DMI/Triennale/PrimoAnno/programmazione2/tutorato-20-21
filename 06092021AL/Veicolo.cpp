#include "Veicolo.h"

using namespace std;

Veicolo::Veicolo() {}

void Veicolo::setCavalli(int cavalli)
{
  this->cavalli=cavalli;    
}

int Veicolo::getCavalli() const
{
    return cavalli;
}

ostream& Veicolo::print(ostream& os) const
{
    os << "cavalli: " << cavalli;
    return os;
}

ostream& operator<<(ostream& os, Veicolo& obj)
{
    return obj.print(os);
}

bool operator>(const Veicolo& obj0, const Veicolo& obj1)
{
    return obj0.getCavalli() > obj1.getCavalli();
}

bool operator>=(const Veicolo& obj0, const Veicolo& obj1)
{
    return obj0.getCavalli() >= obj1.getCavalli();
}

bool operator<=(const Veicolo& obj0, const Veicolo& obj1)
{
    return obj0.getCavalli() <= obj1.getCavalli();
}