#include "Moto.h"

int Moto::getMinCavalli()
{
    return 1;
}

int Moto::getMaxCavalli()
{
    return 50;
}

ostream& operator<<(ostream& os, const Moto& obj)
{
    return obj.print(os);
}

ostream& Moto::print(ostream& os) const
{
    os << "[Moto] ";
    Veicolo::print(os);
    return os;
}
