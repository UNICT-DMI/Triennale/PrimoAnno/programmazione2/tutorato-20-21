#ifndef AUTO_H
#define AUTO_H

#include "Veicolo.h"

class Auto : public Veicolo
{
protected:
    ostream &print(ostream &os) const;

public:
    int getMinCavalli();
    int getMaxCavalli();
    friend ostream& operator<<(ostream&, const Auto&);
};

#endif