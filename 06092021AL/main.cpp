#include <iostream>
#include <cstdlib>
#include <math.h>
#include <typeinfo>

#include "Veicolo.h"
#include "Auto.h"
#include "Barca.h"
#include "Moto.h"

#include "List.h"
#include "Node.h"
#include "Queue.h"
#include "BST.h"

using namespace std;

const int TYPE_AUTO = 0;
const int TYPE_BARCA = 1;
const int TYPE_MOTO = 2;
const int TYPES_AMOUNT = 3;

int randomIntInRange(int min, int max){
    return min + rand() % (max - min + 1);
}

template <typename T>
void printListOfPointers(List<T*>& list) {
    Node<T*>* anchor = list.getHead();

    cout << "Elements pointed: \n";

    while(anchor != NULL) {
        cout << *(anchor->getValue()) << endl;
        anchor = anchor->getNext();
    }

    cout << endl;
}

template <typename T>
BST<T> createBSTFromQueue(Queue<T*>& queue) {
    BST<T> bst;
    while(!queue.isEmpty()) {
        T* pointer = queue.dequeue();
        bst.insert(*pointer);
    }

    return bst;
}

template <typename T> 
void deleteVehiclesWithPowerGreaterThan(BST<T>& bst, int maximumCavalli) {
    BSTNode<T>* anchor = bst.getRoot();
    BSTNode<T>* anchorParent = nullptr;

    while(anchor != nullptr) {
        if(anchor->getKey().getCavalli() <= maximumCavalli) {
            anchorParent = anchor;
            anchor = anchor->getRight();
        } else {
            BSTNode<T>* left = anchor->getLeft();
            bst.replant(anchorParent, anchor, left);
            anchor->setLeft(nullptr);
            bst.deallocateNode(anchor);

            anchor = left;
        }
    }
}

int main(int argc, char const *argv[])
{
    // TODO: Inserire srand
    srand(time(0));

    /*
    PUNTO a:
        Creare N oggetti Veicolo, con N scelto dall’utente, e tipo di Veicolo (auto, moto, barca)
         scelto in modo casuale, così come i valori degli attributi da assegnare ai veicoli,
          tenendo conto delle seguenti caratteristiche:
            •Le auto possono avereun valore di cavalli da 9 a 45;
            •Le barchepossono avereun valore di cavalli da 20 a 55;
            •Le moto possono avere un valore di cavalli da 1 a 50.
            */
    int N;

    cout << "inserisci quanti veicoli vuoi" << endl;
    cin >> N;

    Veicolo* veicoli[N];
    size_t countOfCars = 0;
    size_t countOfShips = 0;
    size_t countOfBikes = 0;

    for (int i = 0; i < N; i++)
    {
        int tipoVeicolo = rand() % TYPES_AMOUNT;

        switch (tipoVeicolo)
        {
        case TYPE_AUTO:
            veicoli[i] = new Auto();
            ++countOfCars;
            break;
        case TYPE_BARCA:
            veicoli[i] = new Barca();
            ++countOfShips;
            break;
        case TYPE_MOTO:
            veicoli[i] = new Moto();
            ++countOfBikes;
            break;
        }
        int min = veicoli[i]->getMinCavalli();
        int max = veicoli[i]->getMaxCavalli();

        veicoli[i]->setCavalli(randomIntInRange(min, max));
    }

    for (int i = 0; i < N;  i++)
    {
        cout << *(veicoli[i]) << endl;
    }

    /*
    PUNTO b
    Inserire gli oggetti Veicolo creati in tre strutture dati dello stesso tipo, 
    una per le istanze di Auto, una per quelle di Moto e la terza per quelle di Barca.    
    */

    Queue<Auto*> carsQueue(countOfCars);
    Queue<Barca*> shipsQueue(countOfShips);
    Queue<Moto*> bikesQueue(countOfBikes);

    for (int i = 0; i < N; i++)
    {
        Veicolo* veicolo = veicoli[i];

        if(typeid(*veicolo) == typeid(Auto))
            carsQueue.enqueue((Auto*) veicolo);

        if(typeid(*veicolo) == typeid(Barca))
            shipsQueue.enqueue((Barca*) veicolo);

        if(typeid(*veicolo) == typeid(Moto))
            bikesQueue.enqueue((Moto*) veicolo);
    }

    /*
    PUNTO c
    Successivamente estrarre gli oggetti dalle tre strutture dati ed inserirli
    in un unico tre altrettanti BST, uno per ciascun tipo di veicolo. 
    */

    BST<Auto> carsBst = createBSTFromQueue<Auto>(carsQueue);
    BST<Barca> shipsBst = createBSTFromQueue<Barca>(shipsQueue);
    BST<Moto> bikesBst = createBSTFromQueue<Moto>(bikesQueue);

    carsBst.inOrderPrint();
    shipsBst.inOrderPrint();
    bikesBst.inOrderPrint();

    /*
    PUNTO d
    Permettere all’utente di inserire un valore di cavalli,  ed  eliminare  dai  tre  BST
    i  veicoli  che hanno una cilindrata superiorea quella data in input dall’utente. 
    */

    int maximumCavalli;
    cout << "inserisci i cavalli massimi" << endl;
    cin >> maximumCavalli;

    deleteVehiclesWithPowerGreaterThan<Auto>(carsBst, maximumCavalli);
    deleteVehiclesWithPowerGreaterThan<Barca>(shipsBst, maximumCavalli);
    deleteVehiclesWithPowerGreaterThan<Moto>(bikesBst, maximumCavalli);

    cout << "--- AUTO" << endl;
    carsBst.inOrderPrint();

    cout << "--- BARCHE" << endl;
    shipsBst.inOrderPrint();

    cout << "--- MOTO" << endl;
    bikesBst.inOrderPrint();

    // Dellocazione dei veicoli
    for (int i = 0; i < N; i++)
    {
        delete veicoli[i];
    }
}