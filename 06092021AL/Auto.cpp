#include "Auto.h"

int Auto::getMinCavalli()
{
    return 9;
}

int Auto::getMaxCavalli()
{
    return 45;
}

ostream& operator<<(ostream& os, const Auto& obj)
{
    return obj.print(os);
}

ostream& Auto::print(ostream& os) const
{
    os << "[Auto] ";
    Veicolo::print(os);
    return os;
}
