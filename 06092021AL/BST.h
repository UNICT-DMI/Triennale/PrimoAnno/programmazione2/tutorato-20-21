#ifndef BST_H
#define BST_H

#include <iostream>

#include "BSTNode.h"

using namespace std;
template <typename T>
class BST {
    private:
        BSTNode<T>* root;

        BSTNode<T>* searchNodeContaining(T);
        void inOrder(BSTNode<T>*);

        size_t countOfElementsOfSubtree(BSTNode<T>*);
    public:
        BST();

        void insert(T);
        void remove(T);
        bool search(T);

        void inOrderPrint();

        size_t countOfElements();

        BSTNode<T>* getRoot();

        void replant(BSTNode<T>*, BSTNode<T>*, BSTNode<T>*);
        void deallocateNode(BSTNode<T>*);

        T extractMin();
        T extractMax();

        ~BST();
};

#include "BST.tpp"

#endif