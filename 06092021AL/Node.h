#ifndef NODE_H
#define NODE_H

template <typename T>
class Node {
    private:
        T value;
        Node<T>* next;

    public:
        Node(T);

        T getValue();

        void setNext(Node*);
        Node* getNext();

        ~Node() { }
};

#include "Node.tpp"

#endif
