#ifndef BSTNODE_H
#define BSTNODE_H

#include <iostream>

using namespace std;

template <typename T>
class BSTNode
{
private:
    T key;
    BSTNode<T> *left;
    BSTNode<T> *right;

public:
    BSTNode(T);

    T getKey() { return key; }

    void setLeft(BSTNode<T> *left) { this->left = left; }
    BSTNode<T> *getLeft() { return this->left; }

    void setRight(BSTNode<T> *right) { this->right = right; }
    BSTNode<T> *getRight() { return this->right; }

    ~BSTNode() {}
};

#include "BSTNode.tpp"

#endif