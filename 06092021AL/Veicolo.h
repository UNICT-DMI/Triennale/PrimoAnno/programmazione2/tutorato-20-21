#ifndef VEICOLO_H
#define VEICOLO_H

#include <iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

class Veicolo
{
protected:
    int cavalli;
    virtual ostream &print(ostream &os) const;

public:
    Veicolo();
    virtual int getMinCavalli() = 0;
    virtual int getMaxCavalli() = 0;
    friend ostream& operator<<(ostream&, Veicolo&);
    friend bool operator>(const Veicolo&, const Veicolo&);
    friend bool operator>=(const Veicolo&, const Veicolo&);
    friend bool operator<=(const Veicolo&, const Veicolo&);

    //setter
    void setCavalli(int);

    //getter
    int getCavalli() const;
};

#endif