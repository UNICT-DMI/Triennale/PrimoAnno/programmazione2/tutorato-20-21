#include "BST.h"

template <typename T>
BST<T>::BST() {
    this->root = nullptr;
}

template <typename T>
BSTNode<T>* BST<T>::getRoot() {
    return this->root;
}

template <typename T>
void BST<T>::insert(T value) {
    BSTNode<T>* node = new BSTNode<T>(value);

    if(root == nullptr) {
        root = node;
        return;
    }

    BSTNode<T>* anchor = root;

    while(true) {
        if(value > anchor->getKey()) {
            if(anchor->getRight() == nullptr) {
                anchor->setRight(node);
                return;
            } else {
                anchor = anchor->getRight();
            }
        } else {
            if(anchor->getLeft() == nullptr) {
                anchor->setLeft(node);
                return;
            } else {
                anchor = anchor->getLeft();
            }
        }
    }
}

template <typename T>
BSTNode<T>* BST<T>::searchNodeContaining(T value) {
    BSTNode<T>* anchor = root;

    while(anchor != nullptr) {
        if(value > anchor->getKey()) {
            anchor = anchor->getRight();
        } else if (value < anchor->getKey()){
            anchor = anchor->getLeft();
        } else {
            return anchor;
        }
    }

    return nullptr;
}

template <typename T>
bool BST<T>::search(T value) {
    return this->searchNodeContaining(value) != nullptr;
}

template <typename T>
void BST<T>::replant(BSTNode<T>* parent, BSTNode<T>* node, BSTNode<T>* replacement) {
    // node non è altro che la radice dell'albero
    if(parent == nullptr) {
        root = replacement;
        return;
    }

    if(parent->getLeft() == node) {
        parent->setLeft(replacement);
    } else {
        parent->setRight(replacement);
    }
}

template <typename T>
void BST<T>::remove(T value) {
    BSTNode<T>* parent = nullptr;
    BSTNode<T>* node = root;

    // Effettua la ricerca del nodo
    while(node != nullptr) {
        if(value > node->getKey()) {
            parent = node;
            node = node->getRight();
        } else if (value < node->getKey()) {
            parent = node;
            node = node->getLeft();
        } else if (value == node->getKey()) {
            break;
        }
    }

    // Il valore non è presente nell'albero
    if(node == nullptr) {
        return;
    }
    
    // Caso 1: Il nodo da rimuovere non ha figli
    if(node->getRight() == nullptr && node->getLeft() == nullptr) {
        replant(parent, node, nullptr);

        delete node;
        return;
    }

    // Caso 2.L: Il nodo ha solo il figlio sinistro
    if(node->getLeft() != nullptr && node->getRight() == nullptr) {
        replant(parent, node, node->getLeft());
        
        delete node;
        return;
    }

    // Caso 2.R: Il nodo ha solo il figlio destro
    if(node->getRight() != nullptr && node->getLeft() == nullptr) {
        replant(parent, node, node->getRight());
        
        delete node;
        return;
    }

    // Caso 3: Il nodo ha due figli
    // Cerchiamo il minimo del sottoalbero destro (successore di 'node')
    BSTNode<T>* successorParent = node;
    BSTNode<T>* successor = node->getRight();

    while(successor->getLeft() != nullptr) {
        successorParent = successor;
        successor = successor->getLeft();
    }

    // Se il successore non è il figlio destro di 'node', troviamo nel suo sottoalbero
    if(successor != node->getRight()) {
        // Il figlio destro del successore diventa il figlio sinistro del suo genitore
        // Viene quindi trapiantato sotto il nuovo minimo (dopo la rimozione di 'successor') del sottoalbero destro
        successorParent->setLeft(successor->getRight());

        successor->setRight(node->getRight());
    }

    // Il figlio sinistro di 'node' può diventare direttamente figlio sinistro di 'successor'
    successor->setLeft(node->getLeft());

    replant(parent, node, successor);

    delete node;
}

template <typename T>
void BST<T>::inOrder(BSTNode<T>* subtreeRoot) {
    if(subtreeRoot == nullptr)
        return;

    inOrder(subtreeRoot->getLeft());
    cout << subtreeRoot->getKey() << " ";
    inOrder(subtreeRoot->getRight());
}

template <typename T>
void BST<T>::inOrderPrint() {
    this->inOrder(this->root);
    cout << endl;
}

template <typename T>
void BST<T>::deallocateNode(BSTNode<T>* node) {
    if(node == nullptr)
        return;

    if(node->getLeft() != nullptr) {
        deallocateNode(node->getLeft());
    }

    if(node->getRight() != nullptr) {
        deallocateNode(node->getRight());
    }

    delete node;
}

template <typename T>
size_t BST<T>::countOfElementsOfSubtree(BSTNode<T>* subtreeRoot) {
    size_t count = 0;

    if(subtreeRoot == nullptr)
        return count;

    count += this->countOfElementsOfSubtree(subtreeRoot->getLeft());
    count += 1;
    count += this->countOfElementsOfSubtree(subtreeRoot->getRight());

    return count;
}

template <typename T>
size_t BST<T>::countOfElements() {
    return this->countOfElementsOfSubtree(this->root);
}

template <typename T>
T BST<T>::extractMin() {
    BSTNode<T>* minimumNode = this->root;
    while(minimumNode->getLeft() != nullptr)
        minimumNode = minimumNode->getLeft();

    T value = minimumNode->getKey();

    this->remove(value);

    return value;
}

template <typename T>
T BST<T>::extractMax() {
    BSTNode<T>* maximumNode = this->root;
    while(maximumNode->getRight() != nullptr)
        maximumNode = maximumNode->getRight();

    T value = maximumNode->getKey();

    this->remove(value);

    return value;
}

template <typename T>
BST<T>::~BST() {
    deallocateNode(root);
}