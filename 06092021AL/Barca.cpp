#include "Barca.h"

int Barca::getMinCavalli()
{
    return 20;
}

int Barca::getMaxCavalli()
{
    return 55;
}

ostream& operator<<(ostream& os, const Barca& obj)
{
    return obj.print(os);
}

ostream& Barca::print(ostream& os) const
{
    os << "[Barca] ";
    Veicolo::print(os);
    return os;
}

