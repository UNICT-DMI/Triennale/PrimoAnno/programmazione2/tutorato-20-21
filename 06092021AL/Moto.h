#ifndef MOTO_H
#define MOTO_H

#include "Veicolo.h"

class Moto : public Veicolo
{
protected:
    ostream &print(ostream &os) const;

public:
    int getMinCavalli();
    int getMaxCavalli();
    friend ostream& operator<<(ostream&, const Moto&);
};

#endif
