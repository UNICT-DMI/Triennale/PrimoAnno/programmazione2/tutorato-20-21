#ifndef BARCA_H
#define BARCA_H

#include "Veicolo.h"

class Barca : public Veicolo
{
protected:
    ostream &print(ostream &os) const;

public:
    int getMinCavalli();
    int getMaxCavalli();
    friend ostream& operator<<(ostream&, const Barca&);
};

#endif