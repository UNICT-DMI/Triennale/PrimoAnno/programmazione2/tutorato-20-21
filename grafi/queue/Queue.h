#ifndef QUEUE_H
#define QUEUE_H

template <typename T>
class Queue {
    private:
        size_t size;
        size_t head, elements_count;
        T* elements;

    public:
        Queue(size_t);

        void enqueue(T);
        T dequeue();

        bool isEmpty();
        bool isFull();

        void print();

        ~Queue();
};

#include "Queue.tpp"

#endif