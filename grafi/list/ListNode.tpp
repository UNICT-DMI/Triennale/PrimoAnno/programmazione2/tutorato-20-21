#include <stddef.h>

#include "ListNode.h"

template <typename T>
ListNode<T>::ListNode(T value) {
   this->value = value; 

   this->next = nullptr;
}

template <typename T>
T ListNode<T>::getValue() {
    return this->value;
}

template <typename T>
void ListNode<T>::setNext(ListNode<T>* next) {
    this->next = next;
}

template <typename T>
ListNode<T>* ListNode<T>::getNext() {
    return this->next;
}
