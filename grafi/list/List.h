#ifndef LIST_H
#define LIST_H

#include "ListNode.h"

template <typename T>
class List {
    private:
        ListNode<T>* head;
    
    public:
        List();

        void insert(T);
        void remove(T);
        bool search(T);

        ListNode<T>* getHead() {
            return this->head;
        };

        void printList();

        ~List();
};

#include "List.tpp"

#endif
