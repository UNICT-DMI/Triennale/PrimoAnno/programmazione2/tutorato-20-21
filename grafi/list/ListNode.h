#ifndef LIST_NODE_H
#define LIST_NODE_H

template <typename T>
class ListNode {
    private:
        T value;
        ListNode<T>* next;

    public:
        ListNode(T);

        T getValue();

        void setNext(ListNode*);
        ListNode* getNext();

        ~ListNode() { }
};

#include "ListNode.tpp"

#endif
