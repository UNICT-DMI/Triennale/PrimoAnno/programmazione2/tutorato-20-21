#include <iostream>

#include "List.h"

using namespace std;

template <typename T>
List<T>::List() {
    this->head = nullptr;
}

template <typename T>
void List<T>::insert(T value) {
    ListNode<T>* node = new ListNode<T>(value);

    if(head == nullptr) {
        head = node;
        return;
    }

    ListNode<T>* anchor = head;

    while(anchor->getNext() != nullptr) {
        anchor = anchor->getNext();
    }

    anchor->setNext(node); 
}

template <typename T>
void List<T>::remove(T value) {
    ListNode<T>* r;

    if(head->getValue() == value) {
        r = head;
        head = head->getNext();
        delete r;

        return;
    }

    ListNode<T>* anchor = head;

    while(anchor->getNext() != nullptr) {
        if(anchor->getNext()->getValue() == value) {
            r = anchor->getNext();
            anchor->setNext(r->getNext());
            delete r;

            return;
        }

        anchor = anchor->getNext(); 
    }
}

template <typename T>
bool List<T>::search(T value) {
    ListNode<T>* anchor = head;

    while(anchor != nullptr) {
        if(anchor->getValue() == value) {
            return true;
        }

        anchor = anchor->getNext();
    }

    return false;
}

template <typename T>
void List<T>::printList() {
    ListNode<T>* anchor = head;

    while(anchor != nullptr) {
        cout << anchor->getValue();
        anchor = anchor->getNext();
    }
}

template <typename T>
List<T>::~List<T>() {
    ListNode<T> *r; 

    while(head != nullptr) {
        r = head;
        head = head->getNext();

        delete r;
    }
}
