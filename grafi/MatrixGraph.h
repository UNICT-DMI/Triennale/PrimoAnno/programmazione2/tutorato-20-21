#ifndef MATRIX_GRAPH_H
#define MATRIX_GRAPH_H

#include "visits/bfs.h"
#include "visits/dfs.h"
#include "visits/topsort.h"

template<typename T>
class MatrixGraph {
    private:
        size_t length;
        size_t firstFreeNodeSlotIndex;

        int findNodeIndex(T);

        T* nodes;
        int** edgesMatrix;

    public:
        MatrixGraph(size_t);

        MatrixGraph<T>* addNode(T);
        MatrixGraph<T>* addEdge(T, T);

        template<typename K>
        friend BFSResult* BFS(MatrixGraph<K>&, K);

        template<typename K>
        friend DFSResult* DFS(MatrixGraph<K>&);

        template<typename K>
        friend void DFSVisit(MatrixGraph<K>&, int, color_t*, uint&, DFSResult*);

        template<typename K>
        friend TopSortResult* TopSort(MatrixGraph<K>&);

        template<typename K>
        friend void TopSortVisit(MatrixGraph<K>&, int, color_t*, uint&, TopSortResult*);

        void print();

        ~MatrixGraph();
};

#include "MatrixGraph.tpp"
#include "visits/bfs_matrix.tpp"
#include "visits/dfs_matrix.tpp"
#include "visits/topsort_matrix.tpp"

#endif