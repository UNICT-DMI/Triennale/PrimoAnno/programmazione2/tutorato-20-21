#include <iostream>

#include "MatrixGraph.h"
#include "ListGraph.h"

#include "visits/bfs.h"
#include "visits/dfs.h"
#include "visits/topsort.h"

using namespace std;

int main() {
    MatrixGraph<char> graph(3);

    graph.addNode('a')->addNode('b')->addNode('c');

    graph.addEdge('c', 'b')->addEdge('b', 'a');

    graph.print();

    TopSortResult* result = TopSort(graph);
    result->print();
    delete result;

    return 0;
}