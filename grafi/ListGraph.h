#ifndef LIST_GRAPH_H
#define LIST_GRAPH_H

#include "list/List.h"

#include "visits/bfs.h"
#include "visits/dfs.h"
#include "visits/topsort.h"

template <typename T>
class ListGraph {
    private:
        size_t length;
        size_t firstFreeNodeSlotIndex;

        T* nodes;
        List<int>** edgesLists;

        int findNodeIndex(T);

    public:
        ListGraph(size_t);

        ListGraph<T>* addNode(T);
        ListGraph<T>* addEdge(T, T);

        template<typename K>
        friend BFSResult* BFS(ListGraph<K>&, K);

        template<typename K>
        friend DFSResult* DFS(ListGraph<K>&);

        template<typename K>
        friend void DFSVisit(ListGraph<K>&, int, color_t*, uint&, DFSResult*);

        template<typename K>
        friend TopSortResult* TopSort(ListGraph<K>&);

        template<typename K>
        friend void TopSortVisit(ListGraph<K>&, int, color_t*, uint&, TopSortResult*);

        void print();

        ~ListGraph();
};

#include "ListGraph.tpp"
#include "visits/bfs_list.tpp"
#include "visits/dfs_list.tpp"
#include "visits/topsort_list.tpp"

#endif