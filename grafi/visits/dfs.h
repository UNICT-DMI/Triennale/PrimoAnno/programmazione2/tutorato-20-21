#ifndef DFS_H
#define DFS_H

typedef int color_t;

#define WHITE 0
#define GRAY 1
#define BLACK 2

class DFSResult {
    private:
        size_t graphSize;

    public:
        uint* d;
        uint* f;
        int* predecessors;

        DFSResult(size_t graphSize) {
            this->graphSize = graphSize;

            d = new uint[graphSize];
            f = new uint[graphSize];
            predecessors = new int[graphSize];
        }

        void print() { 
            cout << "DFS results: " << endl;

            for(size_t i = 0; i < graphSize; ++i) {
                cout << "[" << i << "] ";
                cout << "Discover time: " << d[i] << " ";
                cout << "Finish time: " << f[i] << " ";
                cout << "Predecessor: " << predecessors[i];
                cout << endl;
            }
        }

        ~DFSResult() {
            delete[] d;
            delete[] f;
            delete[] predecessors;
        }
};

#endif