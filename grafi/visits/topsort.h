#ifndef TOPSORT_H
#define TOPSORT_H

typedef int color_t;

#define WHITE 0
#define GRAY 1
#define BLACK 2

class TopSortResult {
    private:
        size_t graphSize;

        int* sortedIndices;
        uint nextSortedIndex;
    public:
        uint* d;
        uint* f;
        int* predecessors;

        TopSortResult(size_t graphSize) {
            this->graphSize = graphSize;

            d = new uint[graphSize];
            f = new uint[graphSize];
            predecessors = new int[graphSize];

            nextSortedIndex = graphSize - 1;
            sortedIndices = new int[graphSize];
        }

        void appendSortedIndex(int index) {
            sortedIndices[nextSortedIndex] = index;
            --nextSortedIndex;
        }

        void print() { 
            cout << "TopSort results: " << endl;

            for(size_t i = 0; i < graphSize; ++i) {
                cout << "[" << i << "] ";
                cout << "Discover time: " << d[i] << " ";
                cout << "Finish time: " << f[i] << " ";
                cout << "Predecessor: " << predecessors[i];
                cout << endl;
            }

            cout << "Sorted indices: ";

            for(size_t i = 0; i < graphSize; ++i) {
                cout << sortedIndices[i] << " ";
            }

            cout << endl;
        }

        ~TopSortResult() {
            delete[] d;
            delete[] f;
            delete[] predecessors;
            delete[] sortedIndices;
        }
};

#endif