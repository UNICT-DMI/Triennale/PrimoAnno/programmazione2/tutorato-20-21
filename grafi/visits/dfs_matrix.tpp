#include "../MatrixGraph.h"

#include "dfs.h"

template <typename T>
DFSResult* DFS(MatrixGraph<T>& graph) {
    DFSResult* result = new DFSResult(graph.length);

    color_t colors[graph.length];

    for(size_t i = 0; i < graph.length; ++i) {
        colors[i] = WHITE;
        result->predecessors[i] = -1;
    }

    uint time = 0;

    for(size_t u = 0; u < graph.length; ++u) {
        if(colors[u] == WHITE) {
            DFSVisit(graph, u, colors, time, result);
        }
    }

    return result;
}

template <typename T>
void DFSVisit(MatrixGraph<T>& graph, int u, color_t* colors, uint& time, DFSResult* result) {
    ++time; 

    colors[u] = GRAY;
    result->d[u] = time;

    for(size_t v = 0; v < graph.length; ++v) {
        bool isAdjacent = graph.edgesMatrix[u][v] == 1;

        if(isAdjacent && colors[v] == WHITE) {
            result->predecessors[v] = u;
            DFSVisit(graph, v, colors, time, result);
        }
    }

    ++time;
    
    colors[u] = BLACK;
    result->f[u] = time;
}
