#include "../ListGraph.h"

#include "dfs.h"

template <typename T>
TopSortResult* TopSort(ListGraph<T>& graph) {
    TopSortResult* result = new TopSortResult(graph.length);

    color_t colors[graph.length];

    for(size_t i = 0; i < graph.length; ++i) {
        colors[i] = WHITE;
        result->predecessors[i] = -1;
    }

    uint time = 0;

    for(size_t u = 0; u < graph.length; ++u) {
        if(colors[u] == WHITE) {
            TopSortVisit(graph, u, colors, time, result);
        }
    }

    return result;
}

template <typename T>
void TopSortVisit(ListGraph<T>& graph, int u, color_t* colors, uint& time, TopSortResult* result) {
    ++time; 

    colors[u] = GRAY;
    result->d[u] = time;

    List<int>* edgesList = graph.edgesLists[u];

    ListNode<int>* currentAdjacentNode = edgesList->getHead();
    while(currentAdjacentNode != nullptr) {
        int v = currentAdjacentNode->getValue();

        if(colors[v] == WHITE) {
            result->predecessors[v] = u;
            TopSortVisit(graph, v, colors, time, result);
        }

        currentAdjacentNode = currentAdjacentNode->getNext();
    }

    ++time;
    
    colors[u] = BLACK;
    result->f[u] = time;

    result->appendSortedIndex(u);
}
