#ifndef BFS_H
#define BFS_H

#include "../queue/Queue.h"

typedef int color_t;

#define WHITE 0
#define GRAY 1
#define BLACK 2

class BFSResult {
    private:
        size_t graphSize;

    public:
        int* distances;
        int* predecessors;

        BFSResult(size_t graphSize) {
            this->graphSize = graphSize;

            distances = new int[graphSize];
            predecessors = new int[graphSize];
        }

        void print() {
            cout << "BFS results: " << endl;

            for(size_t i = 0; i < graphSize; ++i) {
                cout << "[" << i << "]";
                cout << " Distance: " << distances[i];
                cout << " Predecessor: " << predecessors[i];
                cout << endl;
            }
        }
        
        ~BFSResult() {
            delete[] distances;
            delete[] predecessors;
        }
};

#endif