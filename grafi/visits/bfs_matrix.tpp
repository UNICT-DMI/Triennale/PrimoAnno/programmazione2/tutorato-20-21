#include "../MatrixGraph.h"

#include "bfs.h"

template <typename T>
BFSResult* BFS(MatrixGraph<T>& graph, T startValue) {
    BFSResult* result = new BFSResult(graph.length);

    color_t colors[graph.length];

    for(size_t i = 0; i < graph.length; ++i) {
        colors[i] = WHITE;
        result->distances[i] = -1;
        result->predecessors[i] = -1;
    }

    int startIndex = graph.findNodeIndex(startValue);
    colors[startIndex] = GRAY;
    result->distances[startIndex] = 0;

    Queue<int> visitQueue(graph.length);
    visitQueue.enqueue(startIndex);

    while(!visitQueue.isEmpty()) {
        int u = visitQueue.dequeue();

        for(size_t v = 0; v < graph.length; ++v) {
            bool isAdjacent = graph.edgesMatrix[u][v] == 1;

            if(isAdjacent && colors[v] == WHITE) {
                colors[v] = GRAY;
                result->distances[v] = result->distances[u] + 1;
                result->predecessors[v] = u;
                visitQueue.enqueue(v);
            }
        }

        colors[u] = BLACK;
    }

    return result;
}