#include "../ListGraph.h"

#include "bfs.h"

template <typename T>
BFSResult* BFS(ListGraph<T>& graph, T startValue) {
    BFSResult* result = new BFSResult(graph.length);

    color_t colors[graph.length];

    for(size_t i = 0; i < graph.length; ++i) {
        colors[i] = WHITE;
        result->distances[i] = -1;
        result->predecessors[i] = -1;
    }

    int startIndex = graph.findNodeIndex(startValue);
    colors[startIndex] = GRAY;
    result->distances[startIndex] = 0;

    Queue<int> visitQueue(graph.length);
    visitQueue.enqueue(startIndex);

    while(!visitQueue.isEmpty()) {
        int u = visitQueue.dequeue();

        List<int>* edgesList = graph.edgesLists[u];

        ListNode<int>* currentAdjacentNode = edgesList->getHead();
        while(currentAdjacentNode != nullptr) {
            int v = currentAdjacentNode->getValue();

            if(colors[v] == WHITE) {
                colors[v] = GRAY;
                result->distances[v] = result->distances[u] + 1;
                result->predecessors[v] = u;
                visitQueue.enqueue(v);
            }

            currentAdjacentNode = currentAdjacentNode->getNext();
        }

        colors[u] = BLACK;
    }

    return result;
}