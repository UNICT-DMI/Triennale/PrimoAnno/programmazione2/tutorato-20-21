using namespace std;

template <typename T>
MatrixGraph<T>::MatrixGraph(size_t length) {
    this->length = length;
    this->firstFreeNodeSlotIndex = 0;

    this->nodes = new T[length];

    this->edgesMatrix = new int*[length];

    for(size_t i = 0; i < length; ++i) {
        edgesMatrix[i] = new int[length];

        // Assicuriamoci che tutti gli elementi della matrice siano inizialmente 0
        // Nel caso in cui un compilatore non inizializzi questi valori
        // Potremmo trovarci archi indesiderati o valori privi di senso
        for(size_t j = 0; j < length; ++j) {
            edgesMatrix[i][j] = 0;
        }
    }
}

template <typename T>
MatrixGraph<T>::~MatrixGraph() {
    delete[] nodes;

    for(size_t i = 0; i < length; ++i) {
        delete[] edgesMatrix[i];
    }

    delete[] edgesMatrix;
}

template<typename T>
int MatrixGraph<T>::findNodeIndex(T value) {
    for(size_t i = 0; i < length; ++i) {
        if(nodes[i] == value) {
            return i;
        }
    }

    return -1;
}

template<typename T>
MatrixGraph<T>* MatrixGraph<T>::addNode(T value) {
    if(firstFreeNodeSlotIndex >= length) {
        cout << "WARNING: Il grafo è pieno." << endl;
        return this;
    }

    nodes[firstFreeNodeSlotIndex] = value;

    ++firstFreeNodeSlotIndex;

    return this;
}

template<typename T>
MatrixGraph<T>* MatrixGraph<T>::addEdge(T u, T v) {
    int uIndex = findNodeIndex(u);
    int vIndex = findNodeIndex(v);

    if(uIndex == -1 || vIndex == -1) {
        cout << "WARNING: Uno o più valori non presenti nel grafo" << endl;
        return this;
    }

    edgesMatrix[uIndex][vIndex] = 1;

    return this;
}

template <typename T>
void MatrixGraph<T>::print() {
    cout << "Numero di nodi: " << firstFreeNodeSlotIndex << endl;

    for(size_t i = 0; i < length; ++i) {
        cout << "[" << i << "] = " << nodes[i] << endl;
    }

    cout << endl << "Matrice di adiacenza: " << endl;

    cout << "  ";

    for(size_t i = 0; i < length; ++i) {
        cout << nodes[i] << " ";
    }
    cout << endl;

    for(size_t i = 0; i < length; ++i) {
        cout << nodes[i] << " ";

        for(size_t j = 0; j < length; ++j) {
            cout << edgesMatrix[i][j] << " ";
        }

        cout << endl;
    }
}
