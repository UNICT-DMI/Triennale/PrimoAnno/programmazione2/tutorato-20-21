using namespace std;

template <typename T>
ListGraph<T>::ListGraph(size_t length) {
    this->length = length;
    this->firstFreeNodeSlotIndex = 0;

    this->nodes = new T[length];

    this->edgesLists = new List<int>*[length];

    for(size_t i = 0; i < length; ++i) {
        this->edgesLists[i] = new List<int>();
    }
}

template <typename T>
ListGraph<T>::~ListGraph() {
    delete[] nodes;

    for(size_t i = 0; i < length; ++i) {
        delete edgesLists[i];
    }

    delete[] edgesLists;
}

template <typename T>
int ListGraph<T>::findNodeIndex(T value) {
    for(size_t i = 0; i < length; ++i) {
        if(nodes[i] == value) {
            return i;
        }
    }

    return -1;
}

template <typename T>
ListGraph<T>* ListGraph<T>::addNode(T value) {
    if(firstFreeNodeSlotIndex >= length) {
        cout << "WARNING: Il grafo è pieno." << endl;
        return this;
    }

    nodes[firstFreeNodeSlotIndex] = value;

    ++firstFreeNodeSlotIndex;

    return this;
}

template <typename T>
ListGraph<T>* ListGraph<T>::addEdge(T u, T v) {
    int uIndex = findNodeIndex(u);
    int vIndex = findNodeIndex(v);

    if(uIndex == -1 || vIndex == -1) {
        cout << "WARNING: Uno o più valori non presenti nel grafo" << endl;
        return this;
    }

    if(edgesLists[uIndex]->search(vIndex)) {
        cout << "WARNING: Arco già inserito" << endl;
        return this;
    }

    edgesLists[uIndex]->insert(vIndex);

    return this;
}

template <typename T>
void ListGraph<T>::print() {
    cout << "Numero di nodi: " << firstFreeNodeSlotIndex << endl;

    for(size_t i = 0; i < length; ++i) {
        cout << "[" << i << "] = " << nodes[i] << endl;
    }

    cout << "Liste di adiacenza: " << endl;

    for(size_t i = 0; i < length; ++i) {
        cout << "[" << nodes[i] << "]: ";
        edgesLists[i]->printList();
        cout << endl;
    }
}
