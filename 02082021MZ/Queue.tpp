#include <iostream>

#include "Queue.h"

using namespace std;

template <typename T>
Queue<T>::Queue(size_t size) {
    this->size = size;

    this->head = 0;
    this->elements_count = 0;

    this->elements = new T[size];
}

template <typename T>
void Queue<T>::enqueue(T value) {
    elements[(head + elements_count) % size] = value;
    ++elements_count;
}

template <typename T>
T Queue<T>::dequeue() {
    T obj = elements[head];

    --elements_count;
    head = (head + 1) % size;

    return obj;
}

template <typename T>
bool Queue<T>::isEmpty() {
    return elements_count == 0;
}

template <typename T>
bool Queue<T>::isFull() {
    return elements_count == size;
}

template <typename T>
void Queue<T>::print() {
    cout << "| ";

    for(int i = 0; i < elements_count; ++i) {
        cout << elements[(head + i) % size] << " ";
    }

    cout << "| Head: " << head << " Elements count: " << elements_count << endl;
}

template <typename T>
Queue<T>::~Queue() {
    delete elements;
}
