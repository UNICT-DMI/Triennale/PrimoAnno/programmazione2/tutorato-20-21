#include <iostream>
#include <fstream>

#include "Student.h"
#include "List.h"
#include "BST.h"
#include "Queue.h"

using namespace std;

string extractNextFragment(string& line, size_t& currentIndex) {
    size_t currentFragmentEnd = line.find(",", currentIndex);
    if(currentFragmentEnd >= line.length())
        currentFragmentEnd = line.length(); 

    string fragment = line.substr(currentIndex, currentFragmentEnd - currentIndex);
    currentIndex = currentFragmentEnd + 1;

    return fragment;
}

/*
 * Esercizio 1
 * Trovare un modo per non utilizzare la classe CohortBstEntry.
 * Suggerimento: La coorte a cui fa riferimento un albero corrisponde alla coorte dello studente
 * che ha in radice.
 */
class CohortBstEntry {
    private:
        int cohort;
        BST<Student>* bst;

    public:
        CohortBstEntry() { }
        CohortBstEntry(int cohort, BST<Student>* bst) {
            this->cohort = cohort;
            this->bst = bst;
        }

        int getCohort() const {
            return cohort;
        }

        BST<Student>* getBST() const {
            return bst;
        }
};

double totalGradesOfSubtree(BSTNode<Student>* subtreeRoot) {
    double totalGrades = 0.0;

    if(subtreeRoot == NULL)
        return totalGrades;

    totalGrades += totalGradesOfSubtree(subtreeRoot->getLeft());
    totalGrades += subtreeRoot->getKey().getAverageGrades();
    totalGrades += totalGradesOfSubtree(subtreeRoot->getRight());

    return totalGrades;
}

int main() {
    ifstream inputFile("Studenti.txt");

    // 1. Carichi le informazioni contenute nel file in una opportuna struttura dati
    List<Student> students;
    size_t numberOfStudents = 0;

    string line;
    while(getline(inputFile, line)) {
        size_t currentIndex = 0;
        Student student;
        while(currentIndex < line.length()) {
            student.setLastName(extractNextFragment(line, currentIndex)); // Cognome
            student.setFirstName(extractNextFragment(line, currentIndex)); // Nome
            student.setCohort(stoi(extractNextFragment(line, currentIndex))); // Coorte
            student.setStudentId(extractNextFragment(line, currentIndex)); // Matricola
            student.setAverageGrades(stod(extractNextFragment(line, currentIndex))); // Media
        }

        students.insert(student);

        ++numberOfStudents;
    }

    // 2. Estragga le informazioni dalla struttura utilizzata al punto 1. e le inserisca in una coda, in cui ogni elemento  contiene  un  BST  che  raggruppa  tutti  gli  studenti  appartenenti  alla  stessa  coorte;  si scelga un criterio opportuno per l’ordinamento all’interno del BST

    List<CohortBstEntry> bstRegistry;
    size_t numberOfCohorts = 0;

    Node<Student>* currentStudent = students.getHead();

    while(currentStudent != nullptr) {
        int studentCohort = currentStudent->getValue().getCohort();
        BST<Student>* cohortBst = nullptr;

        // Controlliamo se abbiamo già creato il BST relativo alla coorte
        Node<CohortBstEntry>* currentBstEntry = bstRegistry.getHead();
        while(currentBstEntry != nullptr) {
            if(currentBstEntry->getValue().getCohort() == studentCohort) {
                cohortBst = currentBstEntry->getValue().getBST();
                break;
            }

            currentBstEntry = currentBstEntry->getNext();
        }

        // Se alla fine del ciclo cohortBst è null, vuol dire che non abbiamo inizializzato
        // il BST relativo alla coorte.
        // Lo facciamo sul momento e lo inseriamo in bstRegistry
        if(cohortBst == nullptr) {
            cohortBst = new BST<Student>();
            bstRegistry.insert(CohortBstEntry(studentCohort, cohortBst));
            ++numberOfCohorts;
        }
        
        /*
         * Esercizio 2
         * Creare BST di puntatori a studenti (BST<Student*>) invece di BST
         * contenenti i dati dei singoli studenti copiati dalla lista
         */
        // Inseriamo lo studente nel BST relativo alla sua coorte
        cohortBst->insert(currentStudent->getValue());

        currentStudent = currentStudent->getNext(); 
    }

    // Stampa dei BST per debug
    Node<CohortBstEntry>* currentBstEntry = bstRegistry.getHead();
    while(currentBstEntry != nullptr) {
        cout << "Cohort: " << currentBstEntry->getValue().getCohort() << endl;
        currentBstEntry->getValue().getBST()->inOrderPrint();
        currentBstEntry = currentBstEntry->getNext();
    }

    /*
     * Esercizio 3.1
     * Utilizzare una coda implementata tramite lista
     * 
     * Esercizio 3.2
     * Anche qui inserire in coda BST di puntatori a studenti (ref: Esercizio 2)
     */
    // Creiamo la coda di BST
    Queue<BST<Student>*> bstQueue(numberOfCohorts);

    // Inseriamo i BST in coda
    currentBstEntry = bstRegistry.getHead();
    while(currentBstEntry != nullptr) {
        bstQueue.enqueue(currentBstEntry->getValue().getBST());
        currentBstEntry = currentBstEntry->getNext();
    }

    bstQueue.print();

    // 3. Estragga gli elementi dalla coda e, qualora la media della coorte sia superiore o uguale alla media totale degli studenti, li stampi con una visita a piacere

    // Calcoliamo la media totale degli studenti
    double totalAverageGrades = 0;

    currentStudent = students.getHead();

    while(currentStudent != nullptr) {
        totalAverageGrades += currentStudent->getValue().getAverageGrades();
        currentStudent = currentStudent->getNext();
    }

    totalAverageGrades /= numberOfStudents;

    cout << "Total average grades: " << totalAverageGrades << endl;

    while(!bstQueue.isEmpty()) {
        BST<Student>* currentCohort = bstQueue.dequeue();
        int cohort = currentCohort->getRoot()->getKey().getCohort();
        /*
         * Esercizio 4
         * Conservare una cache del numero di studenti di ogni coorte
         * Per poi riutilizzarla nell'eliminazione degli estremi (punto 4) 
         */
        size_t countOfStudents = currentCohort->countOfElements();

        // Calcoliamo la media della coorte
        double totalCohortGrades = totalGradesOfSubtree(currentCohort->getRoot());
        double cohortAverageGrades = totalCohortGrades / countOfStudents;

        cout << "Cohort " << cohort << " average grades: " << cohortAverageGrades << endl;

        if(cohortAverageGrades >= totalAverageGrades) {
            currentCohort->inOrderPrint();
        }
    }

    // 4. Elimini da ogni coorte gli studenti che si posizionano nel 5% inferiore e superiore rispetto al totale degli studenti.Ad esempio: se ci sono 100 studenti, con media da 1 a 100, eliminare gli studenti che hanno media inferiore a 5 o media superiore a 95.

    currentBstEntry = bstRegistry.getHead();
    while(currentBstEntry != nullptr) {
        BST<Student>* currentCohort = currentBstEntry->getValue().getBST();
        int cohort = currentBstEntry->getValue().getCohort();

        size_t countOfStudents = currentCohort->countOfElements();
        size_t studentsToDelete = (size_t) (countOfStudents * 0.05); // Calcolo effettuato per difetto

        cout << "Cohort " << cohort << " stats: " << studentsToDelete << "/" << countOfStudents << endl;

        for(size_t i = 0; i < studentsToDelete; ++i) {
            /*
             * Esercizio 6
             * Implementare versioni più efficienti di extractMin ed extractMax.
             * In particolare, ogni nodo viene cercato nell'albero ben 2 volte.
             * Un metodo che sa quanti minimi/massimi vanno estratti può
             * scendere l'albero una sola volta ed effettuare
             * tutte le estrazioni in un giro
             */
            Student minStudent = currentCohort->extractMin();
            Student maxStudent = currentCohort->extractMax();

            cout << "Minimum student removed: " << minStudent;
            cout << "Maximum student removed: " << maxStudent;
        }

        cout << endl;

        currentBstEntry = currentBstEntry->getNext();
    }

    /* Esercizio 7
     * Dellocare tutte le strutture dati allocate.
     */
    
    return 0;
}
