#include <iostream>

#include "List.h"

using namespace std;

template <typename T>
List<T>::List() {
    this->head = NULL;
}

template <typename T>
void List<T>::insert(T value) {
    Node<T>* node = new Node<T>(value);

    if(head == NULL) {
        head = node;
        return;
    }

    Node<T>* anchor = head;

    while(anchor->getNext() != NULL) {
        anchor = anchor->getNext();
    }

    anchor->setNext(node); 
}

template <typename T>
void List<T>::remove(T value) {
    Node<T>* r;

    if(head->getValue() == value) {
        r = head;
        head = head->getNext();
        delete r;

        return;
    }

    Node<T>* anchor = head;

    while(anchor->getNext() != NULL) {
        if(anchor->getNext()->getValue() == value) {
            r = anchor->getNext();
            anchor->setNext(r->getNext());
            delete r;

            return;
        }

        anchor = anchor->getNext(); 
    }
}

template <typename T>
bool List<T>::search(T value) {
    Node<T>* anchor = head;

    while(anchor != NULL) {
        if(anchor->getValue() == value) {
            return true;
        }

        anchor = anchor->getNext();
    }

    return false;
}

template <typename T>
void List<T>::print() {
    Node<T>* anchor = head;

    cout << "Elements: \n";

    while(anchor != NULL) {
        cout << anchor->getValue();
        anchor = anchor->getNext();
    }

    cout << endl;
}

template <typename T>
List<T>::~List<T>() {
    Node<T> *r; 

    while(head != NULL) {
        r = head;
        head = head->getNext();

        delete r;
    }
}
