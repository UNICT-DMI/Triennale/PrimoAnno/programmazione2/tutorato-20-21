#ifndef LIST_H
#define LIST_H

#include "Node.h"

template <typename T>
class List {
    private:
        Node<T>* head;
    
    public:
        List();

        void insert(T);
        void remove(T);
        bool search(T);

        Node<T>* getHead() {
            return this->head;
        };

        void print();

        ~List();
};

#include "List.tpp"

#endif