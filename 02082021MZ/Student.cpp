#include <iostream>

#include "Student.h"

using namespace std;

Student::Student() { }

void Student::setFirstName(string firstName) {
    this->firstName = firstName;
}

void Student::setLastName(string lastName) {
    this->lastName = lastName;
}

void Student::setStudentId(string studentId) {
    this->studentId = studentId;
}

void Student::setCohort(int cohort) {
    this->cohort = cohort;
}

void Student::setAverageGrades(double averageGrades) {
    this->averageGrades = averageGrades;
}

string Student::getFirstName() const {
    return this->firstName;
}

string Student::getLastName() const {
    return this->lastName;
}

string Student::getStudentId() const {
    return this->studentId;
}

int Student::getCohort() const {
    return this->cohort;
}

double Student::getAverageGrades() const {
    return this->averageGrades;
}

void Student::print(ostream& stream) const {
    stream << this->firstName << " " << this->lastName << " - ";
    stream << this->studentId << " - ";
    stream << this->cohort << " " << this->averageGrades;
    stream << endl;
}

ostream& operator<<(ostream& stream, const Student& student) {
    student.print(stream);
    return stream;
}

bool operator>(const Student& s0, const Student& s1) {
    return s0.getAverageGrades() > s1.getAverageGrades();
}

bool operator<(const Student& s0, const Student& s1) {
    return s0.getAverageGrades() < s1.getAverageGrades();
}

bool operator==(const Student& s0, const Student& s1) {
    return s0.getStudentId() == s1.getStudentId();
}