#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>

using namespace std;

class Student {
    private:
        string firstName;
        string lastName;
        string studentId;
        int cohort;
        double averageGrades; 

    public:
        Student();
        void setFirstName(string);
        void setLastName(string);
        void setStudentId(string);
        void setCohort(int);
        void setAverageGrades(double);

        string getFirstName() const;
        string getLastName() const;
        string getStudentId() const;
        int getCohort() const;
        double getAverageGrades() const;

        void print(ostream&) const;

        friend ostream& operator<<(ostream&,const Student&);
        friend bool operator>(const Student&, const Student&);
        friend bool operator<(const Student&, const Student&);
        friend bool operator==(const Student&, const Student&);
};

#endif