#include "BSTNode.h"

template <typename T>
BSTNode<T>::BSTNode(T key) {
    this->key = key; 
    this->left = NULL;
    this->right = NULL;
}
